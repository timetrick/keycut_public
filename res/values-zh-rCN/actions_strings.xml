<?xml version="1.0" encoding="utf-8"?>
<!--
  ~ Copyright (C) 2015 Kane O'Riley
  ~
  ~ Licensed under the Apache License, Version 2.0 (the "License");
  ~ you may not use this file except in compliance with the License.
  ~ You may obtain a copy of the License at
  ~
  ~ http://www.apache.org/licenses/LICENSE-2.0
  ~
  ~ Unless required by applicable law or agreed to in writing, software
  ~ distributed under the License is distributed on an "AS IS" BASIS,
  ~ WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  ~ See the License for the specific language governing permissions and
  ~ limitations under the License.
  -->

<resources
    xmlns:tools="http://schemas.android.com/tools"
    tools:ignore="MissingTranslation" >

    <string name="app_name_actions">操作</string>

    <string name="menu_activity_picker_label">活动</string>
    <string name="menu_app_picker_label">应用程序</string>
    <string name="menu_shortcut_picker_label">快捷方式</string>

    <string name="apps_category">应用</string>
    <string name="personal_category">个人</string>
    <string name="toggles_category">开关</string>
    <string name="buttons_category">按钮</string>
    <string name="system_category">系统</string>

    <string name="hotword">热词</string>

    <string name="title_select_activity">选择活动</string>
    <string name="title_select_application">选择应用</string>
    <string name="title_select_shortcut">选择快捷方式</string>

    <string name="action_picker_invalid_key">指定了错误的键，正在终止...</string>
    <string name="action_picker_label">操作</string>

    <string name="toggle_airplane">飞行模式</string>
    <string name="toggle_bluetooth">蓝牙</string>
    <string name="toggle_brightness">亮度</string>
    <string name="toggle_flashlight">手电筒</string>
    <string name="toggle_invert">反转显示</string>
    <string name="toggle_location">GPS</string>
    <string name="toggle_mobile_data">移动数据</string>
    <string name="toggle_next">下一曲</string>
    <string name="toggle_nfc">NFC</string>
    <string name="toggle_playpause">播放/暂停</string>
    <string name="toggle_previous">上一曲</string>
    <string name="toggle_ringer_mode">普通/静音</string>
    <string name="toggle_ringer_mode_vibrate">普通/振动</string>
    <string name="toggle_wifi">WiFi</string>
    <string name="toggle_wifiap">热点</string>
    <string name="brightness_down">亮度-</string>
    <string name="brightness_up">亮度+</string>
    <string name="expand_quicksettings">展开快速设置</string>
    <string name="expand_statusbar">展开状态栏</string>
    <string name="reboot">重启对话框</string>
    <string name="recent_apps">最近应用</string>
    <string name="search">搜索</string>
    <string name="voice_search">语音搜索</string>
    <string name="back_key">返回</string>
    <string name="rotate">自动旋转</string>
    <string name="home_key">Home 键</string>
    <string name="menu_key">菜单</string>
    <string name="clear_memory">清理内存</string>
    <string name="power_key">屏幕打开/关闭</string>
    <string name="power_key_root">电源</string>
    <string name="volume_down">音量-</string>
    <string name="volume_up">音量+</string>
    <string name="toolbox">工具箱</string>
    <string name="screenshot">截图</string>
    <string name="shutter">相机快门</string>
    <string name="none">无操作</string>

    <string name="no_root_toast">未在您的系统中找到超级用户软件，此选项不可用</string>
    <string name="not_system_app_toast">此选项需要将 KeyCut 安装到系统分区</string>
    <string name="not_unlocked_toast">此选项需要安装我的解锁包</string>

    <string name="say_the_time">说出时间</string>
    <string name="speak_battery">说出电量</string>

    <string name="say_my_battery_charge_ac">交流电源充电中</string>
    <string name="say_my_battery_charge_usb">USB 充电中</string>
    <string name="say_my_battery_charge_wireless">无线充电中</string>
    <string name="say_my_battery_full">电池已充满</string>
    <string name="say_my_battery_level">电池电量为百分之 %1$s ， %2$s</string>

    <string name="tts_initialise_error">语音数据的文本对您的语言不可用</string>
    <string name="tts_language_not_available">语音数据的文本对您的语言不可用</string>

    <string name="voice_record">录音机</string>
    <string name="voice_record_error_in_call">正在通话时录音机不可用。</string>
    <string name="voice_record_error_internal">录音机发生内部错误。</string>
    <string name="voice_record_error_not_mounted">外置存储未挂载，录音机不可用。</string>
    <string name="notification_recording">正在录音...</string>
    <string name="notification_stopped">录音已停止</string>

    <string name="memory_cleared">后台进程已被结束</string>

    <string name="action_unavailable_no_permission">需要系统权限</string>
    <string name="action_unavailable_not_rooted">需要 Root 权限</string>
    <string name="action_unavailable_not_unlocked">需要解锁包</string>

    <!-- Preferences -->
    <string name="pref_hotword_container">列表</string>
    <string name="pref_hotwords_title">热词</string>
    <string name="pref_title_enable_hotwords">启用热词识别</string>
    <string name="pref_desc_enable_hotwords">通过在主屏幕说出已定义的语句来触发操作</string>
    <string name="pref_title_custom_hotwords">自定义热词</string>
    <string name="pref_desc_custom_hotwords">定义要触发的单词和操作</string>
    <string name="pref_title_disable_hotword_music">当正在播放音乐时禁用</string>
    <string name="pref_desc_disable_hotword_music">当播放音乐时不要侦听热词</string>
    <string name="pref_hotword_add">添加新的热词</string>
    <string name="dialog_hotword_select_action">选择一个操作</string>
    <string name="dialog_hotword_text">语句</string>
    <string name="dialog_hotword_title">自定义热词</string>

    <string name="delete_all">全部删除</string>
    <string name="confirm_delete_all_message">您的热词将全部被删除。</string>
    <string name="confirm_delete_all_title">您确定吗？</string>

    <string name="please_wait">请稍候...</string>
    <string name="say_command">说出您的命令...</string>
    <string name="command_fail">无法识别命令。</string>
    <string name="command_success">正在执行任务...</string>

</resources>
